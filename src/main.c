#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "mem_internals.h"
#include "mem.h"

#define PAGE_SIZE 4096

void debug(const char *fmt, ...);

void test_successful_allocation() {
  debug("Running test_successful_allocation...\n");

  void *heap = heap_init(0);
  void* m1 = _malloc(50);
  void* m2 = _malloc(1);

  assert(m1 != NULL);
  assert(m2 != NULL);

  debug_heap(stderr, heap);
  _free(m1);
  debug_heap(stderr, heap);
  _free(m2);
  debug_heap(stderr, heap);

  (void)m1;(void)m2;
  heap_term();
  debug("_________________________________________________\n");
}

void test_free_one_of_multiple_allocations() {
  debug("Running test_free_one_of_multiple_allocations...\n");

  void *heap = heap_init(0);
  void* m1 = _malloc(30);
  void* m2 = _malloc(20);
  void* m3 = _malloc(145);

  assert(m1 != NULL);
  assert(m2 != NULL);
  assert(m3 != NULL);


  debug_heap(stderr, heap);
  _free(m2);
  debug_heap(stderr, heap);

  (void)m1;(void)m2;(void)m3;
  heap_term();
  debug("_________________________________________________\n");
}

void test_free_multiple_allocations() {
  debug("Running test_free_multiple_allocations...\n");

  void *heap = heap_init(0);
  void* m1 = _malloc(100);
  void* m2 = _malloc(200);
  void* m3 = _malloc(64);
  void* m4 = _malloc(64);

  assert(m1 != NULL);
  assert(m2 != NULL);
  assert(m3 != NULL);
  assert(m4 != NULL);

  debug_heap(stderr, heap);
  _free(m1);
  debug_heap(stderr, heap);
  _free(m3);
  debug_heap(stderr, heap);

  (void)m1;(void)m2;(void)m3;(void)m4;

  heap_term();
  debug("_________________________________________________\n");
}

void test_new_region_expands_old() {
  debug("Running test_new_region_expands_old...\n");

  void *heap = heap_init(0);
  void* m1 = _malloc(PAGE_SIZE);
  void* m2 = _malloc(PAGE_SIZE);

  assert(m1 != NULL);
  assert(m2 != NULL);

  debug_heap(stderr, heap);
  _free(m1);
  debug_heap(stderr, heap);

  (void)m1;(void)m2;
  heap_term();
  debug("_________________________________________________\n");
}

void test_new_region_after_freeing_old() {
  debug("Running test_new_region_after_freeing_old...\n");

  void *heap = heap_init(0);
  void* m1 = _malloc(4040);
  void* m2 = _malloc(PAGE_SIZE);

  assert(m1 != NULL);
  assert(m2 != NULL);

  debug_heap(stderr, heap);
  _free(m1);
  debug_heap(stderr, heap);
  void* m3 = _malloc(5050);
  debug_heap(stderr, heap);

  assert(m3 != NULL);

  (void)m1;(void)m2;(void)m3;
  heap_term();
  debug("_________________________________________________\n");
}

void test_new_region_other_place() {
  debug("Running test_new_region_other_place...\n");

  void *heap = heap_init(0);

  void* m1 = _malloc(PAGE_SIZE);
  void* m2 = _malloc(PAGE_SIZE);
  void* m3 = _malloc(PAGE_SIZE);

  assert(m1 != NULL);
  assert(m2 != NULL);
  assert(m3 != NULL);

  debug_heap(stderr, heap);

  _free(m1);
  _free(m2);

  debug_heap(stderr, heap);

  void* m4 = _malloc(PAGE_SIZE * 2);

  assert(m4 != NULL && m4 != m3);

  debug_heap(stderr, heap);
  (void)m4; (void)m3;
  heap_term();
  debug("_________________________________________________\n");
}

void run_tests() {
  debug("Running tests...\n");
  test_successful_allocation();
  test_free_one_of_multiple_allocations();
  test_free_multiple_allocations();
  test_new_region_expands_old();
  test_new_region_after_freeing_old();
  test_new_region_other_place();
  debug("Tests completed.\n");
}

int main() {
  run_tests();
  return 0;
}
